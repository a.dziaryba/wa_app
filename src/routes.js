import React from 'react';
import { Route, Switch } from 'react-router';

import App from './containers/App';
import HomePage from './components/HomePage';
import SignUpForm from './containers/SignUpForm';
import SignInForm from './containers/SignInForm';
import UserPage from './containers/UserPage';
import Articles from './components/Articles';

import {
  INDEX_PATH,
  SIGN_UP_PATH,
  SIGN_IN_PATH,
  USER_PAGE_PATH,
  ARTICLES_PATH,
} from './constants/RouterConstants';

export default class Routes extends React.Component {
  
  render() {
    return (
      <App>
        <Switch>
          <Route exact path={INDEX_PATH} component={HomePage} />
          <Route exact path={SIGN_UP_PATH} component={SignUpForm} />
          <Route exact path={SIGN_IN_PATH} component={SignInForm} />
          <Route exact path={USER_PAGE_PATH} component={UserPage} />
          <Route exact path={ARTICLES_PATH} component={Articles} />
        </Switch>
      </App>
    );
  }
}