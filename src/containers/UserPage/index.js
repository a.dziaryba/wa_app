import { withRouter } from "react-router";
import UserPage from '../../components/UserPage';
import { connect } from 'react-redux';



export default withRouter(
  connect(
    (state) => ({
      user: state.appState['user'],
    })
  )(UserPage)
);