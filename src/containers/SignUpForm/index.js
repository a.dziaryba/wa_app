import { withRouter } from "react-router";
import { signUp } from '../../actions/appState';
import { connect } from 'react-redux'

import SignUpForm from '../../components/SignUpForm';

export default withRouter(
  connect(
    (state) => ({
      load: state.appState['onLoad'],
    }),
    (dispatch) => ({
      signUp: (email, password) => dispatch(signUp(email, password)),
    })
  )(SignUpForm)
);