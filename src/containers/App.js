import React, { Component } from 'react';
import { connect } from 'react-redux';

import Header from './Header';

class App extends Component {
	render() {
		const { children } = this.props;
		return (
			<div className="wrapper">
				<Header />
				{children}
			</div>
		);
	}
}

export default connect(
    state => ({
        appState: state.appState
    })
)(App);

