import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Header from '../../components/Header';
import { logOut } from '../../actions/appState';

export default withRouter(connect(
	state => ({
    user: state.appState['user'],
	}),
	dispatch => ({
    logOut: (history) => {
      dispatch(logOut(history));
    }
	}),
)(Header));