import { withRouter } from "react-router";
import { signIn } from '../../actions/appState';
import { connect } from 'react-redux';
import { cancelErr } from '../../actions/appState';
import SignInForm from '../../components/SignInForm';


export default withRouter(
  connect(
    state => ({
      error: state.appState['error'],
      load: state.appState['onLoad'],
    }),
    dispatch => ({
      signIn: (email, password) => {
        dispatch(signIn(email, password));
      },
      cancelErr: () => {
        dispatch(cancelErr());
      },
    })
  )(SignInForm)
);