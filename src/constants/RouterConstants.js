export const INDEX_PATH = '/';
export const SIGN_UP_PATH = '/register';
export const SIGN_IN_PATH = '/login';
export const USER_PAGE_PATH = '/user';
export const ARTICLES_PATH = '/articles';
