import React, { Component } from 'react';
import {Grid, Row, Col, Button, ButtonGroup } from 'react-bootstrap';

class Header extends Component {
  async logOut() {
    await this.props.logOut(this.props.history);
  }

  render() {
    return (
      <Grid>
        <Row className="show-grid header">
          <Col className="logo" xs={12} sm={6} md={4} lg={4}>
            <div className="logo">WA app</div>
          </Col>
              {(() => {
                if (this.props.user) {
                  return (
                    <Col xs={3} sm={3} md={3} lg={3} lgPush={5}>
                      <Row className="show-grid">
                        <ButtonGroup className="authButtons">
                          <Button onClick={() => this.props.history.push('/user')}>User info</Button>
                          <Button onClick={() => this.props.history.push('/articles')}>Articles</Button>
                          <Button bsStyle="danger" onClick={this.logOut.bind(this)}>Log out</Button>
                        </ButtonGroup>
                      </Row>
                    </Col>
                  )
                  } else {
                    return (
                      <Col xs={2} sm={2} md={2} lg={2} lgPush={6}>
                        <Row className="show-grid">
                          <ButtonGroup className="authButtons">
                            <Button onClick={() => this.props.history.push('/login')}>Sign in</Button>
                            <Button bsStyle="primary" onClick={() => this.props.history.push('/register')}>Sign up</Button>
                          </ButtonGroup>
                        </Row>
                      </Col>
                    )
                  }
                })()
              }
        </Row>
      </Grid>
    )
  }
}

export default Header;