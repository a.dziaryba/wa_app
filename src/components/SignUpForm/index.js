import React, { Component } from 'react';
import {Grid, Form, Col, Button, FormGroup, ControlLabel, FormControl} from 'react-bootstrap';
import Loader from '../Loader'

class SignUpForm extends Component {
  constructor(props, context) {
    super(props, context);

    this.emailChange = this.emailChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);

    this.state = {
      email: '',
      password: ''
    };
  }

  emailChange(e) {
    this.setState({email: e.target.value});
  }

  passwordChange(e) {
    this.setState({password: e.target.value});
  }

  async signUp() {
    await this.props.signUp(this.state.email, this.state.password);
  }

  render() {
    return (
      <Grid className="sign-up-form">
        <Form horizontal>
          <FormGroup controlId="formHorizontalEmail">
            <Col componentClass={ControlLabel} sm={2}>
              Email
            </Col>
            <Col sm={6}>
              <FormControl type="email" placeholder="Email" onChange={this.emailChange}/>
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalPassword">
            <Col componentClass={ControlLabel} sm={2}>
              Password
            </Col>
            <Col sm={6}>
              <FormControl type="password" placeholder="Password" onChange={this.passwordChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button onClick={!this.props.load ? this.signUp.bind(this) : null}>Sign up</Button>
            </Col>
          </FormGroup>
        </Form>
        { 
          this.props.load ? <Loader /> : null
        }
      </Grid>
      
    )
  }
}

export default SignUpForm;

