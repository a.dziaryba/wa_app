import React, { Component } from 'react';
import {Grid, Form, Col, Button, FormGroup, ControlLabel, FormControl} from 'react-bootstrap';
import Loader from '../Loader/';

class SignInForm extends Component {
  constructor(props, context) {
    super(props, context);

    this.emailChange = this.emailChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);

    this.state = {
      email: '',
      password: ''
    };
  }

  emailChange(e) {
    this.setState({email: e.target.value});
    this.props.cancelErr();
  }

  passwordChange(e) {
    this.setState({password: e.target.value});
  }

  async signIn() {
    await this.props.signIn(this.state.email, this.state.password);
  }

  render() {
    const cancelErr = () => {
      this.props.cancelErr();
      window.removeEventListener('click', cancelErr);
    }

    window.addEventListener('click', cancelErr);

    return (
      <Grid className="sign-up-form">
        <Form horizontal>
          <FormGroup controlId="formHorizontalEmail">
            <Col componentClass={ControlLabel} sm={2}>
              Email
            </Col>
            <Col sm={6}>
              <FormControl type="email" placeholder="Email" onChange={this.emailChange}/>
            </Col>
          </FormGroup>

          <FormGroup controlId="formHorizontalPassword">
            <Col componentClass={ControlLabel} sm={2}>
              Password
            </Col>
            <Col sm={6}>
              <FormControl type="password" placeholder="Password" onChange={this.passwordChange}/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button onClick={!this.props.load ? this.signIn.bind(this) : null}>Sign in</Button>
            </Col>
          </FormGroup>
        </Form>
        {
          (() => {
            if (this.props.load) {
              return <Loader />;
            }
          })()
        }
        <div className="error">
          {
            (() => {
              if (this.props.error) {
                return this.props.error;
              }
            })()
          }
        </div>
      </Grid>
    )
  }
}

export default SignInForm;