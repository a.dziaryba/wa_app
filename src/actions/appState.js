import axios from 'axios';
import appState from '../reducers/appState';
import history from '../history';

export const signUp = (email, password) => {
    return async dispatch => {
        try {
            console.log(email, password) 
            dispatch(appState.actions.set({ field: 'onLoad', value: true }));
            const result = await axios.post('/register', {
                email: email,
                password: password
            });    
            
            if (result) {
                dispatch(appState.actions.set({ field: 'user', value: result.data }));
                history.push('/user');    
            }
        } catch(e) {
            console.log(e);
        }
        dispatch(appState.actions.set({ field: 'onLoad', value: false }));
    }
}

export const logOut = (history) => {
    return async dispatch => {
        dispatch(appState.actions.set({ field: 'user', value: null }));
        history.push('/');   
    }
}

export const signIn = (email, password) => {
    return async dispatch => {
        try {
            dispatch(appState.actions.set({ field: 'onLoad', value: true }));
            const result = await axios.post('/login', {
                email: email,
                password: password
            });    
            if (result.data.type === 'user') {
                dispatch(appState.actions.set({ field: 'user', value: result.data }));
                history.push('/user');               
            }
            
        } catch(e) {
            console.log(e);
            dispatch(appState.actions.set({ field: 'error', value: "Неверный логин или пароль!" }));
        }
        dispatch(appState.actions.set({ field: 'onLoad', value: false }));
    }
}

export const cancelErr = () => {
    return dispatch => {
        dispatch(appState.actions.set({ field: 'error', value: null }));
    }
}

