import  { createModule } from 'redux-modules';

const initialAppState = {
  user: null,
  error: null,
  onLoad: false,
};

export default createModule({
  name: 'appState',
  initialState: initialAppState,
  transformations: {
    set: {
			reducer: (state, { payload }) => {        
        state[payload.field] = payload.value;
        console.log(state);

        return {
          ...state,
          [payload.field]: payload.value
        }
				return state;
      },
    },
  },
})

// export default function appState(appState = initialAppState, action) {
//   switch(action.type) {
//     case 'SET_USER':
//       return {
//         ...appState,
//         user: action.user,
//       }
//       break;
//     case 'HANDLE_ERROR':
//       return {
//         ...appState,
//         error: action.error,
//       }
//       break;
//     case 'LOAD':
//       return {
//         ...appState,
//         onLoad: action.onLoad,
//       }
//       break;
//     default: {
//       return appState
//     }
    
//   }
// }