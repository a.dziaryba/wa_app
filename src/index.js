import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from "react-router-dom";

import Routes from './routes';

import history from './history'
import store from './store'

ReactDOM.render(
  <Provider store = {store}>
    <Router history = {history}>
      <Routes />  
    </Router>
  </Provider>, 
  document.querySelector('#root'));