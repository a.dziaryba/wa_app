const path = require('path');

const config = {
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    }
}

const appConfig = Object.assign({}, config, {
    name: 'app',
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js'
    },
});



module.exports = [
    appConfig     
];