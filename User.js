module.exports = class User {
  constructor(userBody) {
    this.type = 'user';
    this.id = Date.now(),
    this.email = userBody.email,
    this.password = userBody.password
  }
}

